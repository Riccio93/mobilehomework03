using System.Collections;
using UnityEngine;

public class GamePiece : MonoBehaviour
{
    private int x;
    private int y;
    private Grid.PieceType type;
    public Grid grid;
    private AnimalPiece animalComponent;
    private ClearBombSquare clearBombSquareComponent;

    private IEnumerator moveCoroutine;

    //spawn animation
    public AnimationClip spawnAnimation;
    private bool isBeingSpawned = false;
    public bool IsBeingSpawned
    {
        get { return isBeingSpawned; }
    }

    //clear animation
    public AnimationClip clearAnimation;
    private bool isBeingCleared = false;
    public bool IsBeingCleared
    {
        get { return isBeingCleared; }
    }

    private void Awake()
    {
        animalComponent = GetComponent<AnimalPiece>();
        clearBombSquareComponent = GetComponent<ClearBombSquare>();
    }

    public int X
    {
        get { return x; }
        set { x = value; }
    }

    public int Y
    {
        get { return y; }
        set { y = value; }
    }

    public Grid.PieceType Type
    {
        get { return type; }
    }

    public Grid GridReference
    {
        get { return grid; }
    }
    
    public AnimalPiece AnimalComponent
    {
        get { return animalComponent; }
    }

    public ClearBombSquare ClearBombComponent
    { get { return clearBombSquareComponent; } }

    public void Init(int _x, int _y, Grid _grid, Grid.PieceType _type)
    {
        x = _x;
        y = _y;
        grid = _grid;
        type = _type;
    }

    public void Move(int newX, int newY, float moveTime)
    {
        //Stops previous coroutine if another one starts
        if(moveCoroutine != null)
        {
            StopCoroutine(moveCoroutine);
        }

        moveCoroutine = MoveCoroutine(newX, newY, moveTime);
        StartCoroutine(moveCoroutine);
    }

    private IEnumerator MoveCoroutine(int newX, int newY, float moveTime)
    {
        X = newX;
        Y = newY;

        Vector3 startPos = transform.position;
        Vector3 endPos = grid.GetWorldPosition(newX, newY);
        for(float t=0; t <= moveTime; t+=Time.deltaTime)
        {
            transform.position = Vector3.Lerp(startPos, endPos, t / moveTime);
            yield return null;
        }
        //At the end making sure the piece is exactly at the right position
        transform.position = endPos;
        yield return null;
    }

    public bool IsAnimal()
    {
        return animalComponent != null;
    }

    //Probably useless
    public bool IsBomb()
    {
        return clearBombSquareComponent != null;
    }

    //Mouse commands (removed with touch support)
    //void OnMouseEnter()
    //{
    //    grid.EnterPiece(this);
    //}
    //void OnMouseDown()
    //{
    //    grid.PressPiece(this);
    //}
    //void OnMouseUp()
    //{
    //    grid.ReleasePiece();
    //}

    public void Spawn()
    {
        isBeingSpawned = true;
        StartCoroutine(SpawnCoroutine());
    }

    //Plays spawn animation
    private IEnumerator SpawnCoroutine()
    {
        Animator animator = gameObject.GetComponent<Animator>();
        if (animator)
        {
            animator.Play(spawnAnimation.name);
            yield return new WaitForSeconds(spawnAnimation.length);
        }
    }

    public virtual void Clear()
    {
        isBeingCleared = true;
        StartCoroutine(ClearCoroutine());
    }

    //Plays clear animation
    private IEnumerator ClearCoroutine()
    {
        Animator animator = gameObject.GetComponent<Animator>();
        if(animator)
        {
            animator.Play(clearAnimation.name);
            yield return new WaitForSeconds(clearAnimation.length);
            Destroy(gameObject);
        }
    }
}
