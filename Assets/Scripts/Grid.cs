using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
    public Animator ani;
    public bool isPaused = false;

    public enum PieceType
    {
        NORMAL,
        EMPTY,
        BOMB,
        FREEZE,
        COUNT,
    };

    //struct to be "converted" to dict, because dict can't be shown on the Unity inspector
    [System.Serializable] public struct PiecePrefab
    {
        public PieceType type;
        public GameObject prefab;
    };
    public PiecePrefab[] piecePrefabs;
    //piece background alpha value = 155
    public GameObject backgroundPrefab;

    private Dictionary<PieceType, GameObject> piecePrefabDictionary;    

    //Matrix representing the grid
    private GamePiece[,] pieces;

    [SerializeField] private float moveTime;
    private GamePiece pressedPiece;
    private GamePiece enteredPiece;

    //grid dimensions
    [Header("Grid dimensions")]
    public int xDim;
    public int yDim;

    [Header("Power up spawning settings")]
    public int selectedPowerup;
    [SerializeField] private int powerupChance;
    [SerializeField] private float powerupChanceMultiplier;

    [Header("Score settings")]
    public int basePointPerPiece;
    public int moreThenThreeInMatchScoreMultiplier;
    private int currentScore;
    private int finalScore;
    public int bombMultiplier;
    [SerializeField] private Text scoreText;

    [Header("Time Settings")]
    public float startingTime;
    private float currentTime;
    [SerializeField] private Text timeText;
    private bool stopTimer = false;
    public float freezeTime;
    public GameObject iceImage;

    [Header("GameOver Settings")]
    public Text highScoreText;
    public Text finalScoreText;
    public Text newHighScoreText;
    private bool noMovesAvailable = false;
    private bool gameOver = false;

    private void Start()
    {
        newHighScoreText.enabled = false;
        selectedPowerup = PlayerPrefs.GetInt("PowerUp");

        piecePrefabDictionary = new Dictionary<PieceType, GameObject>();

        //Adds in dictionary the struct entries (inserted from Unity inspector)
        for (int i=0; i < piecePrefabs.Length; i++)
        {
            if(!piecePrefabDictionary.ContainsKey(piecePrefabs[i].type))
            {
                piecePrefabDictionary.Add(piecePrefabs[i].type, piecePrefabs[i].prefab);
            }
        }

        //Instantiates the background squares (with orderInLayer = 1 set in editor, sprites have orderInLayer = 0 and appear on top)
        for(int x = 0; x < xDim; x++)
        {
            for(int y = 0; y < yDim; y++)
            {
                GameObject pieceBG = Instantiate(backgroundPrefab, GetWorldPosition(x,y), Quaternion.identity);
                pieceBG.transform.parent = transform;
            }
        }

        //Initiates the grid
        pieces = new GamePiece[xDim,yDim];
        for (int x=0; x<xDim; x++)
        {
            for (int y=0; y<yDim; y++)
            {
                SpawnNewPiece(x, y, PieceType.EMPTY);
            }
        }

        StartCoroutine(Fill());

        currentTime = startingTime;
    }

    public GamePiece SpawnNewPiece(int x, int y, PieceType type)
    {
        GameObject newPiece = Instantiate(piecePrefabDictionary[type], GetWorldPosition(x, y), Quaternion.identity);
        newPiece.transform.parent = transform;

        pieces[x, y] = newPiece.GetComponent<GamePiece>();
        pieces[x, y].Init(x, y, this, type);
        //If the piece type is NORMAL it chooses one animal at random to assign
        if(pieces[x,y].Type == PieceType.NORMAL)
        {
            pieces[x, y].AnimalComponent.SetAnimal((AnimalPiece.AnimalType)Random.Range(0, pieces[x, y].AnimalComponent.NumAnimals));
        }
        //Executes the spawn animation
        pieces[x,y].Spawn();
        return pieces[x, y];
    }

    public IEnumerator Fill()
    {
        bool needsRefill = true;
        while(needsRefill)
        {
            yield return new WaitForSeconds(moveTime);
            for (int x = 0; x < xDim; x++)
            {
                for (int y = 0; y < yDim; y++)
                {
                    GamePiece piece = pieces[x, y];
                    if (piece.Type == PieceType.EMPTY)
                    {
                        SpawnNewPiece(x, y, PieceType.NORMAL);
                        Destroy(piece.gameObject);
                    }
                }
            }
            yield return new WaitForSeconds(moveTime);
            //Checks if another fill is necessary (if I have some new matches)
            needsRefill = ClearAllValidMatches();
            //Checks if there are moves available
            noMovesAvailable = MovesAvailable();
        }        
    }

    private bool MovesAvailable()
    {
        noMovesAvailable = true;
        //For every piece on the board it checks if moving it in one of the 4 direction gives a possible valid match
        for (int x = 0; x < xDim; x++)
        {
            for (int y = 0; y < yDim; y++)
            {
                //If there's a power up on the field there's always a possible move (using the power up)
                if(pieces[x,y].Type == PieceType.BOMB || pieces[x,y].Type == PieceType.FREEZE)
                {
                    noMovesAvailable = false;
                    //Magic number to quit the outer for loop
                    x = 99999;
                    break;
                }
                if(x != 0) //Check left
                {
                    //It swaps the pieces momentarily, checks if there's a possible match, and swaps them back.
                    GamePiece temp = pieces[x, y];
                    pieces[x,y] = pieces[x-1, y];
                    pieces[x - 1, y] = temp;
                    if(GetMatch(pieces[x, y], pieces[x - 1, y].X, pieces[x - 1, y].Y) != null || GetMatch(pieces[x - 1, y], pieces[x, y].X, pieces[x, y].Y) != null)
                    {
                        noMovesAvailable = false;
                    }
                    temp = pieces[x, y];
                    pieces[x, y] = pieces[x - 1, y];
                    pieces[x - 1, y] = temp;

                    if(!noMovesAvailable)
                    {
                        x = 99999;
                        break;
                    }

                }
                if (x != xDim - 1) //Check right
                {
                    GamePiece temp = pieces[x, y];
                    pieces[x, y] = pieces[x + 1, y];
                    pieces[x + 1, y] = temp;

                    if (GetMatch(pieces[x, y], pieces[x + 1, y].X, pieces[x + 1, y].Y) != null || GetMatch(pieces[x + 1, y], pieces[x, y].X, pieces[x, y].Y) != null)
                    {
                        noMovesAvailable = false;
                    }

                    temp = pieces[x, y];
                    pieces[x, y] = pieces[x + 1, y];
                    pieces[x + 1, y] = temp;

                    if (!noMovesAvailable)
                    {
                        x = 99999;
                        break;
                    }
                }
                if (y != 0) //Check up
                {
                    GamePiece temp = pieces[x, y];
                    pieces[x, y] = pieces[x, y - 1];
                    pieces[x, y - 1] = temp;

                    if (GetMatch(pieces[x, y], pieces[x, y - 1].X, pieces[x, y - 1].Y) != null || GetMatch(pieces[x, y - 1], pieces[x, y].X, pieces[x, y].Y) != null)
                    {
                        noMovesAvailable = false;
                    }

                    temp = pieces[x, y];
                    pieces[x, y] = pieces[x, y - 1];
                    pieces[x, y - 1] = temp;

                    if (!noMovesAvailable)
                    {
                        x = 99999;
                        break;
                    }
                }
                if (y != yDim - 1) //Check down
                {
                    GamePiece temp = pieces[x, y];
                    pieces[x, y] = pieces[x, y + 1];
                    pieces[x, y + 1] = temp;

                    if (GetMatch(pieces[x, y], pieces[x, y + 1].X, pieces[x, y + 1].Y) != null || GetMatch(pieces[x, y + 1], pieces[x, y].X, pieces[x, y].Y) != null)
                    {
                        noMovesAvailable = false;
                    }

                    temp = pieces[x, y];
                    pieces[x, y] = pieces[x, y + 1];
                    pieces[x, y + 1] = temp;

                    if (!noMovesAvailable)
                    {
                        x = 99999;
                        break;
                    }
                }
            }
        }
            return noMovesAvailable;
    }
    
    //Gets local (to the grid) position, returns world position
    public Vector2 GetWorldPosition(int x, int y)
    {
        return new Vector2(transform.position.x - xDim / 2.0f + x,
            transform.position.y + yDim / 2.0f - y);
    }

    public bool IsAdjacent(GamePiece piece1, GamePiece piece2)
    {
        return (piece1.X == piece2.X && Mathf.Abs(piece1.Y - piece2.Y) == 1
            || piece1.Y == piece2.Y && Mathf.Abs(piece1.X - piece2.X) == 1);
    }

    public void SwapPieces(GamePiece piece1, GamePiece piece2)
    {
        pieces[piece1.X, piece1.Y] = piece2;
        pieces[piece2.X, piece2.Y] = piece1;

        //Different behaviors if one of the pieces is a power up. First is BOMB
        if (piece1.Type == PieceType.BOMB || piece2.Type == PieceType.BOMB)
        {
            int temp1X = piece1.X;
            int temp1Y = piece1.Y;

            piece1.Move(piece2.X, piece2.Y, moveTime);
            piece2.Move(temp1X, temp1Y, moveTime);

            GamePiece bomb;
            if (piece1.Type == PieceType.BOMB)
                bomb = piece1;
            else
                bomb = piece2;

            int clearPieces = 0;
            //Clears an entire square of pieces (if on border it ignores the ones out of bounds)
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if(bomb.X + x >=0 && bomb.X + x < xDim && bomb.Y + y >= 0 && bomb.Y + y < yDim)
                    {
                        if (pieces[bomb.X + x, bomb.Y + y])
                            ClearPiece(bomb.X + x, bomb.Y + y);
                        clearPieces++;
                    }
                }
            }
            //Special multiplier for destroying pieces with a bomb (set from inspector)
            currentScore += clearPieces * basePointPerPiece * bombMultiplier;

            StartCoroutine(Fill());
        }
        //Special behavior for FREEZE piece
        else if (piece1.Type == PieceType.FREEZE || piece2.Type == PieceType.FREEZE)
        {
            int temp1X = piece1.X;
            int temp1Y = piece1.Y;

            piece1.Move(piece2.X, piece2.Y, moveTime);
            piece2.Move(temp1X, temp1Y, moveTime);

            if(piece1.Type == PieceType.FREEZE)
                ClearPiece(piece1.X, piece1.Y);
            else
                ClearPiece(piece2.X, piece2.Y);

            //Just pauses the timer for a number of seconds
            StartCoroutine(WaitFreeze());

            StartCoroutine(Fill());
        }

        //Only swaps if there's a match
        else if (GetMatch(piece1, piece2.X, piece2.Y) != null || GetMatch(piece2, piece1.X, piece1.Y) != null)
        {
            int temp1X = piece1.X;
            int temp1Y = piece1.Y;

            piece1.Move(piece2.X, piece2.Y, moveTime);
            piece2.Move(temp1X, temp1Y, moveTime);

            ClearAllValidMatches();

            StartCoroutine(Fill());
        }   
        else
        {
            //Otherwise pieces return to their original positions (graphically nothing happens)
            pieces[piece1.X, piece1.Y] = piece1;
            pieces[piece2.X, piece2.Y] = piece2;
        }
    }

    private IEnumerator WaitFreeze()
    {
        stopTimer = true;
        iceImage.SetActive(true);
        //ani.Play("Ice");
        yield return new WaitForSeconds(freezeTime);
        //Goes back to the "central" idle state in the animator so it can call the Game over animation when it's time
        //ani.Play("New State");
        iceImage.SetActive(false);
        stopTimer = false;
    }

    private void Update()
    {
        scoreText.text = currentScore.ToString();
        if (!stopTimer)
        {
            currentTime -= Time.deltaTime;
            timeText.text = currentTime.ToString("F0");
        }

        if (currentTime <= 0 || noMovesAvailable)
        {
            if (!gameOver)
            {
                finalScore = currentScore;
                gameOver = true;
            }
            ShowGameOverScreen();
        }

        //Touch controller
        if(Input.touchCount == 1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            if (Input.GetTouch(0).phase == TouchPhase.Began && !isPaused)
            {                
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    GamePiece gamePiece = hit.transform.gameObject.GetComponent<GamePiece>();
                    EnterPiece(gamePiece);
                }               
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved && !isPaused)
            {
                if (Physics.Raycast(ray, out RaycastHit hit))
                {
                    GamePiece gamePiece = hit.transform.gameObject.GetComponent<GamePiece>();
                    PressPiece(gamePiece);
                }
            }
            else if(Input.GetTouch(0).phase == TouchPhase.Ended && !isPaused)
            {
                if(pressedPiece != null)
                    ReleasePiece();
            }
        }
    }

    //3 Methods to assign the pieces touched by the user
    public void PressPiece(GamePiece piece)
    {
        pressedPiece = piece;
        Debug.Log($"Pressed {piece.X} {piece.Y}");
    }
    public void EnterPiece(GamePiece piece)
    {
        enteredPiece = piece;
        Debug.Log($"Entered {piece.X} {piece.Y}");
    }
    public void ReleasePiece()
    {
        Debug.Log($"Released on {enteredPiece.X} {enteredPiece.Y}");
        if (IsAdjacent(pressedPiece, enteredPiece))
        {
            SwapPieces(pressedPiece, enteredPiece);
        }
    }

    public List<GamePiece> GetMatch(GamePiece piece, int newX, int newY)
    {
        if (piece.IsAnimal())
        {
            AnimalPiece.AnimalType animal = piece.AnimalComponent.Animal;
            List<GamePiece> horizontalPieces = new List<GamePiece>();
            List<GamePiece> verticalPieces = new List<GamePiece>();
            List<GamePiece> matchingPieces = new List<GamePiece>();

            //Check for matches in straight lines (vertical and horizontal)
            //Horizontal checking
            horizontalPieces.Add(piece);

            for(int dir = 0; dir <= 1; dir++)
            {
                for(int xOffset = 1; xOffset < xDim; xOffset++)
                {
                    int x;
                    if(dir == 0) //Left
                    {
                        x = newX - xOffset;
                    }
                    else //Right
                    {
                        x = newX + xOffset;
                    }
                    if (x < 0 || x >= xDim)
                        break;

                    if (pieces[x, newY].IsAnimal() && pieces[x, newY].AnimalComponent.Animal == animal)
                    {
                        horizontalPieces.Add(pieces[x, newY]);
                    }
                    else
                        break;
                }
            }

            if(horizontalPieces.Count >= 3)
            {
                for(int i=0; i<horizontalPieces.Count; i++)
                {
                    matchingPieces.Add(horizontalPieces[i]);
                }
            }

            //L and T shape check
            if (horizontalPieces.Count >= 3)
            {
                for (int i = 0; i < horizontalPieces.Count; i++)
                {
                    for (int dir = 0; dir <= 1; dir++)
                    {
                        for (int yOffset = 1; yOffset < yDim; yOffset++)
                        {
                            int y;
                            if (dir == 0) //Up
                            {
                                y = newY - yOffset;
                            }
                            else //Down
                            {
                                y = newY + yOffset;
                            }
                            if (y < 0 || y >= yDim)
                                break;

                            if (pieces[horizontalPieces[i].X, y].IsAnimal() && pieces[horizontalPieces[i].X, y].AnimalComponent.Animal == animal)
                            {
                                verticalPieces.Add(pieces[horizontalPieces[i].X, y]);
                            }
                            else
                                break;
                        }
                    }
                    //If we don't have enough vertical pieces to form a match, we clear the list for the next tile check
                    if (verticalPieces.Count < 2)
                    {
                        verticalPieces.Clear();
                    }
                    else
                    {
                        for (int j = 0; j < verticalPieces.Count; j++)
                        {
                            matchingPieces.Add(verticalPieces[j]);
                        }
                        break;
                    }
                }
            }

            if (matchingPieces.Count >= 3)
            {
                return matchingPieces;
            }

            //Vertical checking
            horizontalPieces.Clear();
            verticalPieces.Clear();
            verticalPieces.Add(piece);

            for (int dir = 0; dir <= 1; dir++)
            {
                for (int yOffset = 1; yOffset < yDim; yOffset++)
                {
                    int y;
                    if (dir == 0) //Up
                    {
                        y = newY - yOffset;
                    }
                    else //Down
                    {
                        y = newY + yOffset;
                    }
                    if (y < 0 || y >= yDim)
                        break;

                    if (pieces[newX, y].IsAnimal() && pieces[newX, y].AnimalComponent.Animal == animal)
                    {
                        verticalPieces.Add(pieces[newX, y]);
                    }
                    else
                        break;
                }
            }

            if (verticalPieces.Count >= 3)
            {
                for (int i = 0; i < verticalPieces.Count; i++)
                {
                    matchingPieces.Add(verticalPieces[i]);
                }
            }

            //L and T shape check
            if (verticalPieces.Count >= 3)
            {
                for (int i = 0; i < verticalPieces.Count; i++)
                {
                    for (int dir = 0; dir <= 1; dir++)
                    {
                        for (int xOffset = 1; xOffset < xDim; xOffset++)
                        {
                            int x;
                            if (dir == 0) //Left
                            {
                                x = newX - xOffset;
                            }
                            else //Right
                            {
                                x = newX + xOffset;
                            }
                            if (x < 0 || x >= xDim)
                                break;

                            if (pieces[x,verticalPieces[i].Y].IsAnimal() && pieces[x, verticalPieces[i].Y].AnimalComponent.Animal == animal)
                            {
                                horizontalPieces.Add(pieces[x, verticalPieces[i].Y]);
                            }
                            else
                                break;
                        }
                    }
                    //If we don't have enough vertical pieces to form a match, we clear the list for the next tile check
                    if (horizontalPieces.Count < 2)
                    {
                        horizontalPieces.Clear();
                    }
                    else
                    {
                        for (int j = 0; j < horizontalPieces.Count; j++)
                        {
                            matchingPieces.Add(horizontalPieces[j]);
                        }
                        break;
                    }
                }
            }
            if (matchingPieces.Count >= 3)
            {
                return matchingPieces;
            }
        }
        return null;
    }

    public bool ClearPiece(int x, int y)
    {
        if(!pieces[x,y].IsBeingCleared)
        {
            pieces[x, y].Clear();
            SpawnNewPiece(x, y, PieceType.EMPTY);
            return true;
        }
        return false;
    }

    public bool ClearAllValidMatches()
    {
        bool needsRefill = false;

        for(int y=0; y < yDim; y++)
        {
            for(int x=0; x < xDim; x++)
            {
                List<GamePiece> match = GetMatch(pieces[x, y], x, y);
                if(match != null)
                {
                    //Power up selection
                    PieceType powerupType = PieceType.COUNT;
                    GamePiece randomPiece = match[Random.Range(0, match.Count)];
                    int randomNumber = Random.Range(0,100);

                    bool matchHasBomb = false;
                    foreach(GamePiece p in match)
                    {
                        if(p.Type == PieceType.BOMB)
                        {
                            matchHasBomb = true;
                        }
                    }

                    if(!matchHasBomb)
                    {
                        switch(match.Count)
                        {
                            case 3:
                                currentScore += basePointPerPiece * 3;
                                break;
                            case 4:
                                currentScore += basePointPerPiece * 4 * moreThenThreeInMatchScoreMultiplier;
                                break;
                            case 5:
                                currentScore += basePointPerPiece * 5 * moreThenThreeInMatchScoreMultiplier * 2;
                                break;
                            case 6: case 7: case 8: case 9:  case 10: case 11:  case 12: case 13: case 14: case 15:
                                currentScore += basePointPerPiece * 6 * moreThenThreeInMatchScoreMultiplier * 3;
                                break;
                            default:
                                break;
                        }

                        //For example, chance = 20% and multi=2, 3 pieces 20%, 4 pieces 40%, 5 pieces 80% , 6+ pieces 100%
                        if ((match.Count == 3 && randomNumber <= powerupChance) ||
                            (match.Count == 4 && randomNumber <= powerupChance * powerupChanceMultiplier) ||
                            (match.Count == 5 && randomNumber <= powerupChance * Mathf.Pow(powerupChanceMultiplier, 2)) ||
                            (match.Count > 6 && randomNumber <= powerupChance * Mathf.Pow(powerupChanceMultiplier, 3)))
                        {
                            switch (selectedPowerup)
                            {
                                case 0:
                                    powerupType = PieceType.BOMB;
                                    break;
                                case 1:
                                    powerupType = PieceType.FREEZE;
                                    break;
                                default:
                                    break;

                            }
                        }
                    }                    

                    for (int i = 0; i < match.Count; i++)
                    {
                        if(ClearPiece(match[i].X, match[i].Y))
                        {
                            needsRefill = true;
                        }
                    }

                    //if the power up random check goes through, one of the pieces in the match is replaced by the power up
                    if(powerupType != PieceType.COUNT)
                    {
                        Destroy(pieces[randomPiece.X, randomPiece.Y]);
                        //GamePiece newPiece = SpawnNewPiece(randomPiece.X, randomPiece.Y, powerupType);
                        SpawnNewPiece(randomPiece.X, randomPiece.Y, powerupType);
                    }
                }
            }
        }

        return needsRefill;
    }

    public void ShowGameOverScreen()
    {
        finalScoreText.text = "Score : " + finalScore.ToString();
        int hs = PlayerPrefs.GetInt("HighScore");
        if (finalScore > hs)
        {
            PlayerPrefs.SetInt("HighScore", finalScore);
            Debug.Log("New high score!!!");
            newHighScoreText.enabled = true;
        }
        highScoreText.text = "Highscore : " + PlayerPrefs.GetInt("HighScore").ToString();
        ani.SetTrigger("GameOver");
    }
}
