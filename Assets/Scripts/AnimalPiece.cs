using System.Collections.Generic;
using UnityEngine;

public class AnimalPiece : MonoBehaviour
{
    //TODO: is ANY useful?
    public enum AnimalType
    {
        CHICK,
        NARWHAL,
        PIG,
        SLOTH,
        SNAKE,
        ZEBRA,
        ANY,
        COUNT
    }

    [System.Serializable]
    public struct AnimalSprite
    {
        public AnimalType animal;
        public Sprite sprite;
    }
    public AnimalSprite[] animalSprites;
    private SpriteRenderer sprite;
    private Dictionary<AnimalType, Sprite> animalSpriteDictionary;    

    private AnimalType animal;
    public AnimalType Animal
    {
        get { return animal; }
        set { SetAnimal(value); }
    }
    
    public int NumAnimals
    {
        get { return animalSprites.Length; }
    }
        
    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();

        //Dictionary initialization from struct (set in editor)
        animalSpriteDictionary = new Dictionary<AnimalType, Sprite>();
        for(int i=0; i<animalSprites.Length; i++)
        {
            if(!animalSpriteDictionary.ContainsKey(animalSprites[i].animal))
            {
                animalSpriteDictionary.Add(animalSprites[i].animal, animalSprites[i].sprite);
            }
        }
    }

    public void SetAnimal(AnimalType newAnimal)
    {
        animal = newAnimal;
        if(animalSpriteDictionary.ContainsKey(newAnimal))
        {
            sprite.sprite = animalSpriteDictionary[newAnimal];
        }
    }
}
