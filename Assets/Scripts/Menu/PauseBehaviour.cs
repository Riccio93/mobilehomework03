using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseBehaviour : MonoBehaviour
{
    public Animator ani;
    public Animator aniMenu;
    public Grid gridRef;

    public void ShowPauseMenu()
    {
        gridRef.isPaused = true;
        Time.timeScale = 0f;
        ani.SetTrigger("PauseYes");

    }

    public void HidePauseMenu()
    {
        ani.SetTrigger("PauseNo");
        Time.timeScale = 1f;
        gridRef.isPaused = false;
    }

    public void GoToMainMenu() => StartCoroutine(GoToMainMenuCoroutine());

    private IEnumerator GoToMainMenuCoroutine()
    {
        Time.timeScale = 1f;
        aniMenu.SetTrigger("Start");
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(0);
        }
}