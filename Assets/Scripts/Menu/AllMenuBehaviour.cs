using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllMenuBehaviour : MonoBehaviour
{
    [SerializeField] private Animator anim;
    void Start()
    {
        anim.enabled = false;
    }

    public void animOnClick()
    {
        anim.enabled = true;
    }
}
