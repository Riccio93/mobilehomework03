using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float animationLength = 1f;

    public void LoadGameSceneBomb()
    {
        StartCoroutine(LoadGameSceneCoroutine(1, 0));
    }

    public void LoadGameSceneFreeze()
    {
        StartCoroutine(LoadGameSceneCoroutine(1, 1));
    }

    public void LoadMenuScene()
    {
        StartCoroutine(LoadGameSceneCoroutine(0, 0));
    }

    private IEnumerator LoadGameSceneCoroutine(int levelIndex, int powerUp)
    {
        PlayerPrefs.SetInt("PowerUp", powerUp);
        transition.SetTrigger("Start");
        yield return new WaitForSeconds(animationLength);
        SceneManager.LoadScene(levelIndex);
    }
}
